;(function($, window, document, undefined) {
	'use strict';

	main.flipclock = {
		wrapper: '[data-flipclock]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				settings = $wrapper.data('flipclock');

			$wrapper.FlipClock(settings, {
				language: 'pt',
				clockFace: 'DailyCounter',
				countdown: true,
				showSeconds: false
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			}
		}
	};
}(jQuery, this, this.document));

