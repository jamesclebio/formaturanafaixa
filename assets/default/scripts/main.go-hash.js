;(function($, window, document, undefined) {
	'use strict';

	main.goHash = {
		wrapper: 'a[href*="#!/"]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				target = (/#!\//.test(window.location.hash)) ? window.location.hash.replace('!/', '') : false;

			if (target) {
				main.goRoll(target);
			}
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				click: function(e) {
					var	target = $(this).attr('href').replace(/.+#!\//, '#');

					main.goRoll(target);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
