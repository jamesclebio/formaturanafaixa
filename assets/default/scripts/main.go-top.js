;(function($, window, document, undefined) {
	'use strict';

	main.goTop = {
		wrapper: '.go-top',

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				click: function(e) {
					main.goRoll();
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
