<?php
class Password extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
		$this->setTitle('');
		$this->setDescription('');
		$this->setAnalytics(false);
	}

	public function change() {
		$this->setView('password-change');
		$this->setTitle('Alterar senha - Formatura na Faixa');
	}

	public function remember() {
		$this->setView('password-remember');
		$this->setTitle('Lembrar senha - Formatura na Faixa');
	}

	public function reset() {
		$this->setView('password-reset');
		$this->setTitle('Nova senha - Formatura na Faixa');
	}
}

