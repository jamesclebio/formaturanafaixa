<?php
class Sign extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
		$this->setTitle('');
		$this->setDescription('');
		$this->setAnalytics(false);
	}

	public function in() {
		$this->setView('sign-in');
		$this->setTitle('Entrar - Formatura na Faixa');
	}

	public function up() {
		$this->setView('sign-up');
		$this->setTitle('Cadastro - Formatura na Faixa');
	}
}

