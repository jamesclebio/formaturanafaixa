<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/init.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header">
		<div class="global-header-container">
			<h1><a href="<?php echo $this->_url('root') ?>">Formatura na Faixa</a></h1>

			<div class="session">
				<div class="off">
					<form id="form-sign-in" action="">
						<fieldset>
							<legend>Entrar</legend>
							<input type="email" name="user" placeholder="Email">
							<input type="password" name="password" placeholder="Senha">
							<button type="submit">Entrar</button>
							<ul>
								<li>Esqueceu a senha? <a href="<?php echo $this->_url('password/remember') ?>">Clique aqui</a></li>
							</ul>
						</fieldset>
					</form>
				</div>

				<!-- <div class="on">
					<ul>
						<li><a href="#" class="button button-custom-2 button-dropdown"><span class="icon-person"></span> <strong>James Clébio</strong></a>
							<ul class="dropdown">
								<li><a href="<?php echo $this->_url('coupons') ?>">Meus cupoms</a></li>
								<li><a href="<?php echo $this->_url('account') ?>">Meus dados</a></li>
								<li><a href="<?php echo $this->_url('password/change') ?>">Alterar senha</a></li>
								<li class="divider"></li>
								<li><a href="#">Sair</a></li>
							</ul>
						</li>
						<li><a href="#" class="button button-custom-1"><strong>Sair</strong></a></li>
					</ul>
				</div> -->
			</div>
		</div>
	</header>

	<nav class="global-nav">
		<ul>
			<li class="on"><a href="<?php echo $this->_url('root') ?>">Home</a></li>
			<li><a href="<?php echo $this->_url('sign/up') ?>">Cadastro</a></li>
			<li><a href="<?php echo $this->_url('rule') ?>">Regulamento</a></li>
			<li><a href="<?php echo $this->_url('winner') ?>">Vencedor</a></li>
		</ul>
	</nav>

	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<footer class="global-footer">
		<div class="links">
			<h4><a href="http://www.nivelsuperior.com.br/" target="_blank">www.nivelsuperior.com.br</a> | <a href="tel:07932115006">79 3211-5006</a></h4>

			<ul class="social">
				<li class="facebook"><a href="https://www.facebook.com/nivelsuperiorformaturas" target="_blank" title="Facebook">/nivelsuperiorformaturas</a></li>
				<li class="instagram"><a href="http://instagram.com/nivelsuperiorformaturas" target="_blank" title="Instagram">@nivelsuperiorformaturas</a></li>
				<li class="youtube"><a href="https://www.youtube.com/user/nsformaturas" target="_blank" title="YouTube">/nsformaturas</a></li>
			</ul>
		</div>

		<a href="http://www.nivelsuperior.com.br/" target="_blank" class="logo-nivelsuperior">Nível Superior Formaturas</a></li>
	</footer>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>