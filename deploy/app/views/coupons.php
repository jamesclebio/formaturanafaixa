<section class="section-content">
	<div class="section-content-container">
		<header>
			<h2>Meus cupoms (5)</h2>
		</header>

		<!-- <div class="alert alert-error" data-alert-close="true">
			<p><strong>Alert message here!</strong></p>
			<p>Secondary alert message here.</p>
		</div> -->

		<form id="form-coupon" method="post" action="" class="form">
			<fieldset>
				<legend>Cupom</legend>
				<div class="block-pane-bordered">
					<h4 class="heading-pane">Novo cupom</h4>
					<div class="field-merged">
						<input name="coupon" type="text" placeholder="Digite aqui o código do cupom" required>
						<button type="submit" class="button">Adicionar cupom</button>
					</div>
				</div>
			</fieldset>
		</form>

		<table class="table margin-top-30">
			<thead>
				<tr>
					<th>Cupom</th>
					<th>Adicionado</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><span class="text-xlarge"><strong>0123456789</strong></span></td>
					<td>Em 12/12/1212, as 20:20:20</td>
				</tr>
				<tr>
					<td><span class="text-xlarge"><strong>0123456789</strong></span></td>
					<td>Em 12/12/1212, as 20:20:20</td>
				</tr>
				<tr>
					<td><span class="text-xlarge"><strong>0123456789</strong></span></td>
					<td>Em 12/12/1212, as 20:20:20</td>
				</tr>
				<tr>
					<td><span class="text-xlarge"><strong>0123456789</strong></span></td>
					<td>Em 12/12/1212, as 20:20:20</td>
				</tr>
				<tr>
					<td><span class="text-xlarge"><strong>0123456789</strong></span></td>
					<td>Em 12/12/1212, as 20:20:20</td>
				</tr>
			</tbody>
		</table>
	</div>
</section>
