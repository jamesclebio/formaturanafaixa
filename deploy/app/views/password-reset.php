<section class="section-content">
	<div class="section-content-container">
		<header>
			<h2>Redefinir senha</h2>
		</header>

		<!-- <div class="alert alert-error" data-alert-close="true">
			<p><strong>Alert message here!</strong></p>
			<p>Secondary alert message here.</p>
		</div> -->

		<form id="form-password" method="post" action="" class="form">
			<fieldset>
				<legend>Senha</legend>
				
				<div class="grid grid-items-2">
					<div class="grid-item">
						<label>Nova senha *<input name="password" type="password" required></label>
						<label>Confirme a nova senha *<input name="password_confirm" type="password" required></label>
					</div>
				</div>

				<div class="block-action">
					<button type="submit" class="button">Confirmar</button>
				</div>
			</fieldset>
		</form>
	</div>
</section>
