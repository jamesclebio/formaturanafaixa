<section class="section-content">
	<div class="section-content-container">
		<header>
			<h2>Entrar</h2>
		</header>

		<div class="block-compact-center">
			<div class="alert alert-error" data-alert-close="true">
				<p><strong>Dados de acesso inválidos!</strong></p>
				<p>Verifique as informações e tente novamente.</p>
			</div>

			<form id="form-sign-in" method="post" action="" class="form">
				<fieldset>
					<legend>Entrar</legend>
					<label class="label-field-large-icon">
						<span class="icon-person"></span>
						<input name="user" type="text" placeholder="Usuário (Email)" class="field-large" required>
					</label>
					<label class="label-field-large-icon">
						<span class="icon-locked"></span>
						<input name="password" type="password" placeholder="Senha" class="field-large" required>
					</label>
					<div class="block-action">
						<ul>
							<li><a href="<?php echo $this->_url('password/remember'); ?>">Esqueceu a senha?</a></li>
						</ul>
						<button type="submit" class="button button-large">Entrar</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</section>
