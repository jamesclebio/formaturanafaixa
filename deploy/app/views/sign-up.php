<section class="section-content" id="cadastro">
	<div class="section-content-container">
		<header>
			<h2>Cadastro</h2>
		</header>

		<!-- <div class="alert alert-error" data-alert-close="true">
			<p><strong>Alert message here!</strong></p>
			<p>Secondary alert message here.</p>
		</div> -->

		<form id="form-account" method="post" action="" class="form">
			<fieldset>
				<legend>Cadastro</legend>
				<div class="grid grid-items-2">
					<div class="grid-item">
						<h4 class="heading-pane">Dados pessoais</h4>
						<label>Nome completo *<input name="nome" type="text" required></label>
						<label>CPF *<input name="cpf" type="text" class="mask-cpf" required></label>
						<label>RG *<input name="rg" type="text" required></label>
						<label>E-mail *<input name="email" type="email" required></label>
						<label>Telefone *<input name="telefone" type="text" class="mask-phone" required></label>
						<div class="grid grid-items-2">
							<div class="grid-item">
								<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
							</div>
							<div class="grid-item">
								<label>Sexo *
									<select name="sexo" required>
										<option value="m">Masculino</option>
										<option value="f">Feminino</option>
									</select>
								</label>
							</div>
						</div>
						<label>Nova senha *<input name="senha" type="password" required></label>
						<label>Confirme a nova senha *<input name="senha_conf" type="password" required></label>
					</div>

					<div class="grid-item">
						<h4 class="heading-pane">Endereço</h4>
						<label>CEP *<input name="cep" type="text" class="mask-zipcode" required></label>
						<label>Endereço *<input name="endereco" type="text" required></label>
						<label>Bairro *<input name="bairro" type="text" required></label>
						<label>Número *
							<input name="numero" type="text" required>
							<!-- <span class="field-note">Caso não possua, digite SN.</span> -->
							<!-- <ul class="errorlist">
								<li>Lorem ipsum dolor</li>
							</ul> -->
						</label>
						<label>Complemento<input name="complemento" type="text"></label>
						<div class="grid grid-items-2">
							<div class="grid-item">
								<label>Cidade *<input name="cidade" type="text" required></label>
							</div>
							<div class="grid-item">
								<label>Estado *
									<select name="estado" required>
										<option value="AC">Acre</option>
										<option value="AL">Alagoas</option>
										<option value="AP">Amapá</option>
										<option value="AM">Amazonas</option>
										<option value="BA">Bahia</option>
										<option value="CE">Ceará</option>
										<option value="DF">Distrito Federal</option>
										<option value="ES">Espírito Santo</option>
										<option value="GO">Goiás</option>
										<option value="MA">Maranhão</option>
										<option value="MT">Mato Grosso</option>
										<option value="MS">Mato Grosso do Sul</option>
										<option value="MG">Minas Gerais</option>
										<option value="PA">Pará</option>
										<option value="PB">Paraíba</option>
										<option value="PR">Paraná</option>
										<option value="PE">Pernambuco</option>
										<option value="PI">Piauí</option>
										<option value="RJ">Rio de janeiro</option>
										<option value="RN">Rio Grande do Norte</option>
										<option value="RS">Rio Grande do Sul</option>
										<option value="RO">Rondônia</option>
										<option value="RR">Roraima</option>
										<option value="SC">Santa Catarina</option>
										<option value="SP">São Paulo</option>
										<option value="SE">Sergipe</option>
										<option value="TO">Tocantins</option>
									</select>
								</label>
							</div>
						</div>

						<h4 class="heading-pane">Pergunta</h4>
						<div class="label">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, autem?</div>
						<label class="check"><input name="question" type="radio" value="1" checked required> Option A</label>
						<label class="check"><input name="question" type="radio" value="2"> Option B</label>
					</div>
				</div>

				<div class="separate">
					<label class="check"><input name="aceite" type="checkbox" value="1" required>Li e aceito as regras descritas no <a href="<?php echo $this->_url('rule') ?>" target="_blank" class="link">regulamento</a>.</label>
				</div>

				<div class="block-action">
					<button type="submit" class="button">Confirmar cadastro</button>
				</div>
			</fieldset>
		</form>
	</div>
</section>

